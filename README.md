Slaygon is a rpg dungeon crawler created in 1988.
Published by Microdeal and created by _John Conley_ and _James Oxley_ for the Atari, and later ported to Amiga by Timothy Purves.

## Development

_We created Slaygon because most RPG style maze games at the time were lacking. You could not save your game without backtracking out of a dungeon and you had to use graph paper to map the dungeon. We resolved these and many other issues and Slaygon was actually very ahead of its time in many ways. I am still very proud of our accomplishment. Later we developed “Day of the Viper” when technology caught up with our ambitions and it was a much better game._ - John Conley

## Open Source

Like most other games from that period this game has been available (without permission) at various sites for many years, but in 2018 it was clearly given permission by the authors to be released under an open license. This is part an ongoing effort to release the old Microdeal/Michtron-titles for legal preservation.

Sadly, any source code for the game *might* have been lost in time, so all we can offer at the moment is the image files for usage in an emulator. However, the authors is looking through their archives. Also, we are in contact with the Amiga porter of the game.

## License

See LICENSE file for usage license!


## Help!!

- Are you a former Microdeal/Michtron contact or worker and happen have an old backup of the source code for any of the platforms? Please  help us and send it to our email. It is totally fine to send it anonymously.

## Many thanks to 

John Symes for being positive about the project and giving permission, The Dorsman Family for letting the Michtron rights revert to their authors, John Conley and James Oxley for being supportive of releasing the game and for creating a game ahead of its time.

## References

[Hall of light (Amiga)](http://hol.abime.net/1974)

[Atari Mania](http://www.atarimania.com/game-atari-st-slaygon_11247.html)

[Slaygon Page](https://amigasourcepres.gitlab.io/page/s/slaygon/)
